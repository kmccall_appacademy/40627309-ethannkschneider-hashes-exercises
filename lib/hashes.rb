# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  words = str.split
  word_lengths_hash = Hash.new(0)

  words.each { |word| word_lengths_hash[word] = word.length }

  word_lengths_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |key, val| val }[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each { |key, val| older[key] = val }
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letters = Hash.new(0)
  word.each_char { |ch| letters[ch] += 1 }
  letters
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hash = Hash.new(0)
  arr.each { |el| hash[el] += 1 }
  hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  parity_hash = Hash.new(0)
  numbers.each do |num|
    if num.even?
      parity_hash[:even] += 1
    else
      parity_hash[:odd] += 1
    end
  end

  parity_hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowel_hash = Hash.new(0)
  vowels = "aeiou"

  string.each_char { |ch| vowel_hash[ch] += 1 if vowels.include?(ch) }
  vowel_arr = vowel_hash.sort_by { |key, val| val }
  max_freq = vowel_arr[-1][-1]

  vowel_hash.select! { |key, val| val == max_freq }
  vowel_hash.sort[0][0]

end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  students.reject! { |key, val| val < 7 }
  #now we have an array
  student_names = students.keys
  result = []
  student_names.each_with_index do |student, index|
    counter = index + 1
    while counter < student_names.length
      result << [student, student_names[counter]] unless result.include?([student, student_names[counter]])
      counter += 1
    end
  end
  result
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  counter_hash = Hash.new(0)
  specimens.each { |el| counter_hash[el] += 1 }
  number_of_species = counter_hash.keys.count
  populations_arr = counter_hash.values
  smallest_population_size = populations_arr.sort[0]
  largest_population_size = populations_arr.sort[-1]
  (number_of_species**2) * (smallest_population_size / largest_population_size)
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_sign_letters = character_count(normal_sign)
  vandalized_sign_letters = character_count(vandalized_sign)
  vandalized_sign_letters.all? do |key, val|
    val <= normal_sign_letters[key]
  end
end

def character_count(str)
  counter_hash = Hash.new(0)
  str.downcase.each_char { |ch| counter_hash[ch] += 1 }
  counter_hash
end
